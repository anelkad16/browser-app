package com.example.browserapp

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.example.browserapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var url: String
    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        val appTitle = getString(R.string.app_name)
        binding.searchButton.setOnClickListener {
            url = binding.editTextView.text.toString()
            webView.loadUrl(url)
        }
        webView = WebView(this)
        binding.webViewContainer.addView(webView)

        webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                val title = view?.title
                binding.toolbar.title = if (title.isNullOrEmpty()) appTitle else title
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val menuInflater = menuInflater
        val orientation = resources.configuration.orientation
        when (orientation) {
            Configuration.ORIENTATION_PORTRAIT -> menuInflater.inflate(R.menu.menu_main_portrait, menu)
            Configuration.ORIENTATION_LANDSCAPE -> menuInflater.inflate(R.menu.menu_main_landscape, menu)
            else -> Unit
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.action_color_blue -> {
            binding.searchButton.setBackgroundColor(Color.BLUE)
            true
        }

        R.id.action_color_violet -> {
            binding.searchButton
                .setBackgroundColor(getColor(R.color.violet_color))
            true
        }

        R.id.action_color_green -> {
            binding.searchButton.setBackgroundColor(Color.GREEN)
            true
        }

        R.id.action_color_brown -> {
            binding.searchButton
                .setBackgroundColor(getColor(R.color.brown_color))
            true
        }

        R.id.action_color_yellow -> {
            binding.searchButton.setBackgroundColor(Color.YELLOW)
            true
        }

        R.id.action_color_orange -> {
            binding.searchButton
                .setBackgroundColor(getColor(R.color.orange_color))
            true
        }

        else -> false
    }

}
